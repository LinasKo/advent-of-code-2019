from copy import deepcopy


def memory_from_string(string):
    return list(map(int, string.rstrip().split(",")))


def string_from_memory(mem):
    return ",".join(map(str, mem))


def dissect_instruction_head(instr):
    string = str(instr)
    opcode = int(string[-2:])
    if opcode == 99:
        param_mode_length = 0
    elif opcode == 1 or opcode == 2 or opcode == 7 or opcode == 8:
        param_mode_length = 3
    elif opcode == 3 or opcode == 4:
        param_mode_length = 1
    elif opcode == 5 or opcode == 6:
        param_mode_length = 2
    else:
        raise ValueError(f"Unrecognized opcode '{opcode}'")

    param_modes_immediate = []
    for i in range(1, param_mode_length+1):
        rev_index = -2 - i
        if -rev_index > len(string):
            param_modes_immediate.append(False)
        else:
            param_modes_immediate.append(bool(int(string[-2-i])))

    return opcode, param_modes_immediate


def next_state(mem, addr):
    """
    Resolve one iteration of the program, updating memory in place.

    :param mem: list(int), array of numbers, will be updated in-place
    :param addr: int, address to start reading from
    :return: Position to start reading from next time or -1
             if the program terminated successfully.
    """
    opcode, modes = dissect_instruction_head(mem[addr])
    # print(f"{mem[addr:addr+4]}==({opcode}, {modes}), at {addr}")

    if opcode == 99:
        return -1

    elif opcode == 1 or opcode == 2:
        m1 = mem[addr+1] if modes[0] else mem[mem[addr+1]]
        m2 = mem[addr+2] if modes[1] else mem[mem[addr+2]]
        assert not modes[2]; write_addr = mem[addr+3]
        if opcode == 1:
            mem[write_addr] = m1 + m2
        else:
            mem[write_addr] = m1 * m2

    elif opcode == 3:
        assert not modes[0]; write_addr = mem[addr+1]
        mem[write_addr] = int(input("Input a number: ").rstrip())

    elif opcode == 4:
        out_val = mem[addr+1] if modes[0] else mem[mem[addr+1]]
        print("Output:", out_val)

    elif opcode == 5 or opcode == 6:
        condition = mem[addr+1] if modes[0] else mem[mem[addr+1]]
        jump_addr = mem[addr+2] if modes[1] else mem[mem[addr+2]]
        if opcode == 5:
            if condition != 0:
                return jump_addr
        elif opcode == 6:
            if condition == 0:
                return jump_addr
        else:
            raise ValueError(f"Jump error")

    elif opcode == 7 or opcode == 8:
        p1 = mem[addr+1] if modes[0] else mem[mem[addr+1]]
        p2 = mem[addr+2] if modes[1] else mem[mem[addr+2]]
        assert not modes[2]; write_addr = mem[addr+3]
        if opcode == 7:
            mem[write_addr] = int(p1 < p2)
        else:
            mem[write_addr] = int(p1 == p2)

    else:
        raise ValueError(f"Unrecognized opcode '{opcode}'")

    return addr + len(modes) + 1


def resolve(mem):
    address = 0
    while address != -1:
        address = next_state(mem, address)


# Solution
with open("input.txt", "r") as fh:
    lines = fh.readlines()
assert len(lines) == 1
mem = memory_from_string(lines[0])

resolve(mem)
