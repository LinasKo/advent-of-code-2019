RANGE_START = 353096
RANGE_END = 843212

def check_number(num):
    string = str(num)
    
    # Check if increasing
    for i in range(1, len(string)):
        if string[i-1] > string[i]:
            return False

    # Check if repeating at least once
    found_group_of_2 = False
    group_length = 1
    for i in range(1, len(string)):
        if string[i-1] == string[i]:
            group_length += 1
        else:
            if group_length == 2:
                found_group_of_2 = True
            group_length = 1

    if group_length == 2:
        found_group_of_2 = True

    return found_group_of_2

if __name__ == "__main__":
    nums_found = 0
    for num in range(RANGE_START, RANGE_END+1):
        nums_found += check_number(num)
    print(nums_found)