from copy import deepcopy
import enum


class Mode(enum.Enum):
    Position = "0"
    Immediate = "1"
    Relative = "2"


class Opcode(enum.Enum):
    Sum = 1
    Product = 2
    Input = 3
    Output = 4
    JumpIfNonzero = 5
    JumpIfZero = 6
    LessThan = 7
    Equals = 8
    ChangeRelativeBase = 9
    Terminate = 99


def get_address(mem, ip, offset, mode):
    """
    :param mem: memory
    :param ip: int, instruction pointer, representing current address
    :param offset: int, offset from the instruction pointer to read the current value
    :param mode: Mode, parameter mode
    """
    if mode == Mode.Position.value:
        new_addr = mem[ip + offset]
    elif mode == Mode.Immediate.value:
        new_addr = ip + offset
    elif mode == Mode.Relative.value:
        new_addr = relative_base + mem[ip + offset]
    else:
        raise ValueError("Mode %d not recognized" % mode)

    if new_addr < 0:
        raise ValueError(f"Negative address produced.")
    return new_addr


def memory_from_string(string):
    memory = {}
    for i, val in enumerate(string.rstrip().split(",")):
        memory[i] = int(val)
    return memory


def dissect_instruction_head(instr):
    string = str(instr)
    opcode = int(string[-2:])
    if opcode == Opcode.Terminate.value:
        param_mode_length = 0

    elif opcode in [Opcode.Sum.value, Opcode.Product.value, Opcode.LessThan.value, Opcode.Equals.value]:
        param_mode_length = 3

    elif opcode in [Opcode.Input.value, Opcode.Output.value, Opcode.ChangeRelativeBase.value]:
        param_mode_length = 1

    elif opcode in [Opcode.JumpIfNonzero.value, Opcode.JumpIfZero.value]:
        param_mode_length = 2

    else:
        raise ValueError(f"Unrecognized opcode {opcode}")

    param_modes = []
    for i in range(1, param_mode_length+1):
        rev_index = -2 - i
        if -rev_index > len(string):
            param_modes.append(Mode.Position.value)
        else:
            param_modes.append(string[-2-i])

    return opcode, param_modes


def next_state(mem, addr, output_array):
    """
    Resolve one iteration of the program, updating memory in place.

    :param mem: list(int), array of numbers, will be updated in-place
    :param addr: int, address to start reading from
    :return: Position to start reading from next time or -1
             if the program terminated successfully.
    """
    global relative_base
    opcode, modes = dissect_instruction_head(mem[addr])

    # # Debug
    # print(f"--- --- ---")
    # print(f"Mem: {mem}")
    # print(f"addr: {addr}, Op: {opcode}, Modes: {modes}, Rel base: {relative_base}")

    if opcode == Opcode.Terminate.value:
        return -1

    elif opcode == Opcode.Sum.value or opcode == Opcode.Product.value:
        m1 = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        m2 = mem.get(get_address(mem, addr, 2, modes[1]), 0)

        assert modes[2] != Mode.Immediate.value
        write_addr = get_address(mem, addr, 3, modes[2])

        if opcode == Opcode.Sum.value:
            mem[write_addr] = m1 + m2
        else:
            mem[write_addr] = m1 * m2

    elif opcode == Opcode.Input.value:
        assert modes[0] != Mode.Immediate.value
        write_addr = get_address(mem, addr, 1, modes[0])
        mem[write_addr] = int(input("Input a number: ").rstrip())

    elif opcode == Opcode.Output.value:
        out_val = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        output_array.append(out_val)

    elif opcode == Opcode.JumpIfNonzero.value or opcode == Opcode.JumpIfZero.value:
        condition = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        jump_addr = mem.get(get_address(mem, addr, 2, modes[1]), 0)

        if opcode == Opcode.JumpIfNonzero.value:
            if condition != 0:
                return jump_addr
        else:
            if condition == 0:
                return jump_addr

    elif opcode == Opcode.LessThan.value or opcode == Opcode.Equals.value:
        p1 = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        p2 = mem.get(get_address(mem, addr, 2, modes[1]), 0)

        assert modes[2] != Mode.Immediate.value
        write_addr = get_address(mem, addr, 3, modes[2])

        if opcode == Opcode.LessThan.value:
            mem[write_addr] = int(p1 < p2)
        else:
            mem[write_addr] = int(p1 == p2)

    elif opcode == Opcode.ChangeRelativeBase.value:
        relative_base += mem.get(get_address(mem, addr, 1, modes[0]), 0)

    else:
        raise ValueError(f"Unrecognized opcode '{opcode}'")

    return addr + len(modes) + 1


def resolve(mem, output_array):
    address = 0
    while address != -1:
        address = next_state(mem, address, output_array)

def run_incode_computer(line):
    relative_base = 0
    mem = memory_from_string(line)
    output_array = []
    resolve(mem, output_array)
    return output_array


# Solution
with open("input.txt", "r") as fh:
    lines = fh.readlines()
assert len(lines) == 1, len(lines)
line = lines[0]

run_incode_computer(line)

