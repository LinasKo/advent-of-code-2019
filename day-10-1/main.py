import heapq
from math import gcd


def read_in_asteroids(lines):
    map_shape = (len(lines), len(lines[0].rstrip()))
    asteroids_set = set()
    for y in range(map_shape[0]):
        for x in range(map_shape[1]):
            if lines[y][x] == "#":
                asteroids_set.add((y, x))

    return asteroids_set, map_shape


def in_map(cell, map_shape):
    y, x = cell
    return y >= 0 and y < map_shape[0] and x >= 0 and x < map_shape[1]


def compute_least_blocked(asteroids_set, map_shape):
    """An algorithm for determining which asteroid can see the most others"""

    # Make an asteroids lsit for index-based access, alias the set, it will be used to check
    # if a cell has an asteroid.
    ASTEROIDS_SET = asteroids_set 
    ASTEROIDS_LIST = list(asteroids_set)

    # Push all asteroids into a min priority queue
    # which will penalize asteroids by how many others can't be seen.
    # Heapify won't be called as all values are the same for now.
    pqueue_asteroids_blocked = [(0, asteroid) for asteroid in ASTEROIDS_SET]

    # Each asteroid will also keep sets of visible and blocked asteroids,
    # and an index of asteroid in ALL_ASTEROIDS to look at next. 
    visible = {}
    blocked = {}
    next_asteroid_to_examine = {}
    for asteroid in ASTEROIDS_SET:
        visible[asteroid] = set()
        blocked[asteroid] = set()
        next_asteroid_to_examine[asteroid] = 0

    # Tun till an answer is found
    while True: 
        priority, asteroid = heapq.heappop(pqueue_asteroids_blocked)

        # Select next asteroid (improvement possible by first choosing nearby asteroids)
        while next_asteroid_to_examine[asteroid] < len(ASTEROIDS_LIST):
            next_asteroid = ASTEROIDS_LIST[next_asteroid_to_examine[asteroid]]
            if next_asteroid == asteroid or next_asteroid in visible[asteroid] or next_asteroid in blocked[asteroid]:
                next_asteroid_to_examine[asteroid] += 1
            else:
                break
        else:
            break

        # Compute gcd to see if any others could exist between
        diff_y, diff_x = next_asteroid[0] - asteroid[0], next_asteroid[1] - asteroid[1]
        diff_gcd = gcd(diff_y, diff_x)
        step_y, step_x = diff_y / diff_gcd, diff_x / diff_gcd

        # Mark the closest as visible and others as blocked.
        next_cell = (asteroid[0] + step_y, asteroid[1] + step_x)
        next_visible = True
        while in_map(next_cell, map_shape):
            if next_cell in ASTEROIDS_SET:
                if next_visible:
                    visible[asteroid].add(next_cell)
                    next_visible = False
                else:
                    blocked[asteroid].add(next_cell)

            next_cell = (next_cell[0] + step_y, next_cell[1] + step_x)

        # Re-insert into the queue
        heapq.heappush(pqueue_asteroids_blocked, (len(blocked[asteroid]), asteroid))

    return asteroid, len(visible[asteroid])


# Test 1
lines = (
    ".#..#\n"
    ".....\n"
    "#####\n"
    "....#\n"
    "...##\n"
).split()
asteroids_set, map_shape = read_in_asteroids(lines)
asteroid, visible_asteroids = compute_least_blocked(asteroids_set, map_shape)
assert visible_asteroids == 8

# Test 2
lines = (
    "......#.#.\n"
    "#..#.#....\n"
    "..#######.\n"
    ".#.#.###..\n"
    ".#..#.....\n"
    "..#....#.#\n"
    "#..#....#.\n"
    ".##.#..###\n"
    "##...#..#.\n"
    ".#....####\n"
).split()
asteroids_set, map_shape = read_in_asteroids(lines)
asteroid, visible_asteroids = compute_least_blocked(asteroids_set, map_shape)
assert visible_asteroids == 33

# Test 3
lines = (
    "#.#...#.#.\n"
    ".###....#.\n"
    ".#....#...\n"
    "##.#.#.#.#\n"
    "....#.#.#.\n"
    ".##..###.#\n"
    "..#...##..\n"
    "..##....##\n"
    "......#...\n"
    ".####.###.\n"
).split()
asteroids_set, map_shape = read_in_asteroids(lines)
asteroid, visible_asteroids = compute_least_blocked(asteroids_set, map_shape)
assert visible_asteroids == 35

# Test 4
lines = (
    ".#..#..###\n"
    "####.###.#\n"
    "....###.#.\n"
    "..###.##.#\n"
    "##.##.#.#.\n"
    "....###..#\n"
    "..#.#..#.#\n"
    "#..#.#.###\n"
    ".##...##.#\n"
    ".....#.#..\n"
).split()
asteroids_set, map_shape = read_in_asteroids(lines)
asteroid, visible_asteroids = compute_least_blocked(asteroids_set, map_shape)
assert visible_asteroids == 41

# Test 5
lines = (
    ".#..##.###...#######\n"
    "##.############..##.\n"
    ".#.######.########.#\n"
    ".###.#######.####.#.\n"
    "#####.##.#.##.###.##\n"
    "..#####..#.#########\n"
    "####################\n"
    "#.####....###.#.#.##\n"
    "##.#################\n"
    "#####.##.###..####..\n"
    "..######..##.#######\n"
    "####.##.####...##..#\n"
    ".#####..#.######.###\n"
    "##...#.##########...\n"
    "#.##########.#######\n"
    ".####.#.###.###.#.##\n"
    "....##.##.###..#####\n"
    ".#.#.###########.###\n"
    "#.#.#.#####.####.###\n"
    "###.##.####.##.#..##\n"
).split()
asteroids_set, map_shape = read_in_asteroids(lines)
asteroid, visible_asteroids = compute_least_blocked(asteroids_set, map_shape)
assert visible_asteroids == 210


# Solution
with open("input.txt", "r") as fh:
    lines = fh.readlines()

asteroids_set, map_shape = read_in_asteroids(lines)
asteroid, visible_asteroids = compute_least_blocked(asteroids_set, map_shape)
print(f"Least blocked: {asteroid}, that can see {visible_asteroids} others.")
