def read_layers(line, size_xy):
	layers = []

	layer = [["." for _ in range(size_xy[0])] for _ in range(size_xy[1])]

	x = 0
	y = 0
	for char in line:
		layer[y][x] = char
		x += 1
		if x == size_xy[0]:
			y += 1
			x = 0
		if y == size_xy[1]:
			y = 0
			layers.append(layer)
			layer = [["." for _ in range(size_xy[0])] for _ in range(size_xy[1])]

	return layers

def count_nums(layer, num_char):
	count = 0
	for row in layer:
		for elem in row:
			count += elem == num_char
	return count

with open("input.txt", "r") as fh:
	lines = fh.readlines()
assert len(lines) == 1
img_string = lines[0].rstrip()
IMG_XY = (25, 6)
layers = read_layers(img_string, IMG_XY)

min_zeroes = float("inf")
min_layer = None
for layer in layers:
	zeroes_count = count_nums(layer, "0")
	if zeroes_count < min_zeroes:
		min_zeroes = zeroes_count
		min_layer = layer


print(count_nums(min_layer, "1") * count_nums(min_layer, "2"))
		