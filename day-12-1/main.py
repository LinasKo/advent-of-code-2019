import re


def read_input(lines):
    positions = []
    for line in lines:
        match = re.match(r"<x=(.*), y=(.*), z=(.*)>", line)
        pos = match.groups(0)
        pos = [int(pos[0]), int(pos[1]), int(pos[2])]
        positions.append(pos)
    velocities = [[0, 0, 0] for _ in range(len(positions))]
    return positions, velocities


def update_velocities(positions, velocities):
    for p1 in range(len(positions)):
        for p2 in range(len(positions)):
            if p1 != p2:
                for dim in range(len(positions[0])):
                    val1, val2 = positions[p1][dim], positions[p2][dim]
                    if val1 < val2:
                        velocities[p1][dim] += 1
                    elif val1 > val2:
                        velocities[p1][dim] -= 1


def update_positions(positions, velocities):
    for p1 in range(len(positions)):
        for dim in range(len(positions[0])):
            positions[p1][dim] += velocities[p1][dim]


def compute_energy(positions, velocities):
    total = 0
    for position, velocity in zip(positions, velocities):
        potential = sum(map(abs, position))
        kinetic = sum(map(abs, velocity))
        total += potential * kinetic
    return total


# Test 1
lines = [
    "<x=-1, y=0, z=2>",
    "<x=2, y=-10, z=-7>",
    "<x=4, y=-8, z=8>",
    "<x=3, y=5, z=-1>"
]
positions, velocities = read_input(lines)
STEPS = 10
for _ in range(STEPS):
    update_velocities(positions, velocities)
    update_positions(positions, velocities)

total_energy = compute_energy(positions, velocities)
assert total_energy == 179, total_energy


# Test 2
lines = [
    "<x=-8, y=-10, z=0>",
    "<x=5, y=5, z=10>",
    "<x=2, y=-7, z=3>",
    "<x=9, y=-8, z=-3>"
]
positions, velocities = read_input(lines)
STEPS = 100
for _ in range(STEPS):
    update_velocities(positions, velocities)
    update_positions(positions, velocities)

total_energy = compute_energy(positions, velocities)
assert total_energy == 1940, total_energy


# Solution
with open("input.txt", "r") as fh:
    lines = fh.readlines()
positions, velocities = read_input(lines)
STEPS = 1000
for _ in range(STEPS):
    update_velocities(positions, velocities)
    update_positions(positions, velocities)

total_energy = compute_energy(positions, velocities)
print("Total energy:", total_energy)
