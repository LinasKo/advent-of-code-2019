def memory_from_string(string):
    return list(map(int, string.rstrip().split(",")))

def string_from_memory(mem):
    return ",".join(map(str, mem))

def next_state(mem, add):
    """
    Resolve one iteration of the program, updating memory in place.

    :param mem: list(int), array of numbers, will be updated in-place
    :param add: int, address to start reading from
    :return: Position to start reading from next time or -1
             if the program terminated successfully.
    """
    cmd = mem[add]
    if cmd == 99:
        return -1
    elif cmd == 1:
        mem[mem[add+3]] = mem[mem[add+1]] + mem[mem[add+2]]
        return add + 4
    elif cmd == 2:
        mem[mem[add+3]] = mem[mem[add+1]] * mem[mem[add+2]]
        return add + 4
    else:
        raise ValueError(f"Incorrect command '{cmd}'")

def resolve(mem):
    address = 0
    while address != -1:
        address = next_state(mem, address)


# Tests
mem = memory_from_string("1,0,0,0,99")
resolve(mem)
assert string_from_memory(mem) == "2,0,0,0,99", string_from_memory(mem)

mem = memory_from_string("2,3,0,3,99")
resolve(mem)
assert string_from_memory(mem) == "2,3,0,6,99"

mem = memory_from_string("2,4,4,5,99,0")
resolve(mem)
assert string_from_memory(mem) == "2,4,4,5,99,9801"

mem = memory_from_string("1,1,1,4,99,5,6,0,99")
resolve(mem)
assert string_from_memory(mem) == "30,1,1,4,2,5,6,0,99"

# Solution
with open("input.txt", "r") as fh:
    lines = fh.readlines()
assert len(lines) == 1
mem = memory_from_string(lines[0])

mem[1] = 12
mem[2] = 2

resolve(mem)

print(mem)
print(f"Data at address 0: {mem[0]}")