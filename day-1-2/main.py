def fuel_req_recursive(mass):
    fuel = (int(mass) // 3) - 2
    if fuel >= 0:
        return fuel + fuel_req_recursive(fuel)
    return max(0, fuel)

assert fuel_req_recursive(14) == 2
assert fuel_req_recursive(1969) == 966
assert fuel_req_recursive(100756) == 50346

total_cost = 0
with open("input.txt", "r") as fh:
    line = fh.readline()
    while line:
        line = line.rstrip()
        
        total_cost += fuel_req_recursive(line)

        line = fh.readline()

print(f"Total cost: {total_cost}")
