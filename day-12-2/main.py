import re
from copy import copy
from math import gcd


def read_input(lines):
    positions = None
    for line in lines:
        match = re.match(r"<x=(.*), y=(.*), z=(.*)>", line)
        pos = match.groups(0)

        if not positions:
            positions = [[] for _ in pos]

        for i in range(len(pos)):
            positions[i].append(int(pos[i]))

    velocities = [[0] * len(positions[0]) for _ in positions]

    return positions, velocities


def step(x, dx):
    new_x, new_dx = copy(x), copy(dx)
    for i in range(len(x)):
        for j in range(len(x)):
            if i != j:
                if x[i] < x[j]:
                    new_dx[i] += 1
                elif x[i] > x[j]:
                    new_dx[i] -= 1

    for i in range(len(x)):
        new_x[i] += new_dx[i]

    return new_x, new_dx

def find_period(orig_x, orig_dx):
    x, dx = copy(orig_x), copy(orig_dx)
    ordered_set = {}

    index = 0
    ordered_set[tuple(x + dx)] = index
    while True:
        index += 1
        x, dx = step(x, dx)
        x_dx = tuple(tuple(x + dx))
        if x_dx in ordered_set:
            break
        ordered_set[x_dx] = index

    return ordered_set[x_dx], index


# Test 1
lines = [
    "<x=-8, y=-10, z=0>",
    "<x=5, y=5, z=10>",
    "<x=2, y=-7, z=3>",
    "<x=9, y=-8, z=-3>"
]
positions, velocities = read_input(lines)
x_offset, x_period = find_period(positions[0], velocities[0])
y_offset, y_period = find_period(positions[1], velocities[1])
z_offset, z_period = find_period(positions[2], velocities[2])
# print("Offsets:", x_offset, y_offset, z_offset)  # All 0 = easy case. Unclear if I have this bit of the solution right
# print("Periods:", x_period, y_period, z_period)

offset = max([x_offset, y_offset, z_offset])

# I don't get why. Damnit.
x_period //= gcd(x_period, y_period)
x_period //= gcd(x_period, z_period)
# y_period //= gcd(y_period, z_period)

res = offset + x_period * y_period * z_period
assert res == 4686774924, f"res: {res}, res / answ: {res / 4686774924}"


# Solution
with open("input.txt", "r") as fh:
    lines = fh.readlines()
positions, velocities = read_input(lines)
x_offset, x_period = find_period(positions[0], velocities[0])
y_offset, y_period = find_period(positions[1], velocities[1])
z_offset, z_period = find_period(positions[2], velocities[2])
# print("Offsets:", x_offset, y_offset, z_offset)  # All 0 = easy case. Unclear if I have this bit of the solution right
print("Periods:", x_period, y_period, z_period)

offset = max([x_offset, y_offset, z_offset])

# I don't get why. Damnit.
x_period //= gcd(x_period, y_period)
x_period //= gcd(x_period, z_period)
y_period //= gcd(y_period, z_period)  # LOL.

res = offset + x_period * y_period * z_period
print(res)
