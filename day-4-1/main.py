RANGE_START = 353096
RANGE_END = 843212

def check_number(num):
    string = str(num)
    
    # Check if increasing
    for i in range(1, len(string)):
        if string[i-1] > string[i]:
            return False

    # Check if repeating at least once
    for i in range(1, len(string)):
        if string[i-1] == string[i]:
            return True
    else:
        return False

if __name__ == "__main__":
    nums_found = 0
    for num in range(RANGE_START, RANGE_END+1):
        nums_found += check_number(num)
    print(nums_found)