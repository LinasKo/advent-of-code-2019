from copy import deepcopy
import enum
from itertools import permutations


class Mode(enum.Enum):
    Position = "0"
    Immediate = "1"
    Relative = "2"


class Opcode(enum.Enum):
    Sum = 1
    Product = 2
    Input = 3
    Output = 4
    JumpIfNonzero = 5
    JumpIfZero = 6
    LessThan = 7
    Equals = 8
    ChangeRelativeBase = 9
    Terminate = 99


class BoosterIoManager:
    def __init__(self, input_list):
        assert len(input_list) == 5
        self.input_list = input_list
        self.input_pointer = 0
        self.output_list = [0]
        self.provide_output = False

    def get(self):
        if self.provide_output or self.input_pointer >= len(self.input_list):
            ret = self.output_list[-1]

        else:
            ret = self.input_list[self.input_pointer]
            self.input_pointer += 1

        # Will continue to flip after we run out of inputs. Doesn't matter.
        self.provide_output = not self.provide_output

        return ret

    def add_output(self, val):
        self.output_list.append(val)


def get_address(mem, ip, offset, mode):
    """
    :param mem: memory
    :param ip: int, instruction pointer, representing current address
    :param offset: int, offset from the instruction pointer to read the current value
    :param mode: Mode, parameter mode
    """
    if mode == Mode.Position.value:
        new_addr = mem[ip + offset]
    elif mode == Mode.Immediate.value:
        new_addr = ip + offset
    elif mode == Mode.Relative.value:
        new_addr = relative_base + mem[ip + offset]
    else:
        raise ValueError("Mode %d not recognized" % mode)

    if new_addr < 0:
        raise ValueError(f"Negative address produced.")
    return new_addr


def memory_from_string(string):
    memory = {}
    for i, val in enumerate(string.rstrip().split(",")):
        memory[i] = int(val)
    return memory


def dissect_instruction_head(instr):
    string = str(instr)
    opcode = int(string[-2:])
    if opcode == Opcode.Terminate.value:
        param_mode_length = 0

    elif opcode in [Opcode.Sum.value, Opcode.Product.value, Opcode.LessThan.value, Opcode.Equals.value]:
        param_mode_length = 3

    elif opcode in [Opcode.Input.value, Opcode.Output.value, Opcode.ChangeRelativeBase.value]:
        param_mode_length = 1

    elif opcode in [Opcode.JumpIfNonzero.value, Opcode.JumpIfZero.value]:
        param_mode_length = 2

    else:
        raise ValueError(f"Unrecognized opcode {opcode}")

    param_modes = []
    for i in range(1, param_mode_length+1):
        rev_index = -2 - i
        if -rev_index > len(string):
            param_modes.append(Mode.Position.value)
        else:
            param_modes.append(string[-2-i])

    return opcode, param_modes


def next_state(mem, addr, booster_io_manager):
    """
    Resolve one iteration of the program, updating memory in place.

    :param mem: list(int), array of numbers, will be updated in-place
    :param addr: int, address to start reading from
    :param booster_io_manager: IoManager, that passes in inputs and records outputs
    :return: tuple (addr, halt) - Position to start reading from next time or -1
             if the program terminated successfully, and a flag whether to pause the execution.
    """
    global relative_base
    opcode, modes = dissect_instruction_head(mem[addr])

    # # Debug
    # print(f"--- --- ---")
    # print(f"Mem: {mem}")
    # print(f"addr: {addr}, Op: {opcode}, Modes: {modes}, Rel base: {relative_base}")

    if opcode == Opcode.Terminate.value:
        return -1, True

    elif opcode == Opcode.Sum.value or opcode == Opcode.Product.value:
        m1 = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        m2 = mem.get(get_address(mem, addr, 2, modes[1]), 0)

        assert modes[2] != Mode.Immediate.value
        write_addr = get_address(mem, addr, 3, modes[2])

        if opcode == Opcode.Sum.value:
            mem[write_addr] = m1 + m2
        else:
            mem[write_addr] = m1 * m2

    elif opcode == Opcode.Input.value:
        assert modes[0] != Mode.Immediate.value
        write_addr = get_address(mem, addr, 1, modes[0])
        mem[write_addr] = booster_io_manager.get()

    elif opcode == Opcode.Output.value:
        out_val = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        booster_io_manager.add_output(out_val)
        return addr + len(modes) + 1, True

    elif opcode == Opcode.JumpIfNonzero.value or opcode == Opcode.JumpIfZero.value:
        condition = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        jump_addr = mem.get(get_address(mem, addr, 2, modes[1]), 0)

        if opcode == Opcode.JumpIfNonzero.value:
            if condition != 0:
                return jump_addr, False
        else:
            if condition == 0:
                return jump_addr, False

    elif opcode == Opcode.LessThan.value or opcode == Opcode.Equals.value:
        p1 = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        p2 = mem.get(get_address(mem, addr, 2, modes[1]), 0)

        assert modes[2] != Mode.Immediate.value
        write_addr = get_address(mem, addr, 3, modes[2])

        if opcode == Opcode.LessThan.value:
            mem[write_addr] = int(p1 < p2)
        else:
            mem[write_addr] = int(p1 == p2)

    elif opcode == Opcode.ChangeRelativeBase.value:
        relative_base += mem.get(get_address(mem, addr, 1, modes[0]), 0)

    else:
        raise ValueError(f"Unrecognized opcode '{opcode}'")

    return addr + len(modes) + 1, False


def resolve(mem, start_address, booster_io_manager):
    address = start_address
    while address != -1:
        address, halt = next_state(mem, address, booster_io_manager)
        if halt:
            return address
    return address


# Test 1
line = "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5"

final_results = []
for perm in [[9, 8, 7, 6, 5]]:
    
    mems = [memory_from_string(line) for _ in range(5)]
    mem_addresses = [0] * len(mems)
    booster_io_manager = BoosterIoManager(perm)

    current_mem_index = 0

    while mem_addresses[-1] != -1:
        mem = mems[current_mem_index]
        addr = mem_addresses[current_mem_index]
        new_addr = resolve(mem, addr, booster_io_manager)

        mem_addresses[current_mem_index] = new_addr
        current_mem_index = (current_mem_index + 1) % len(mems)

    final_results.append(booster_io_manager.output_list[-1])

assert len(final_results) == 1 and final_results[0] == 139629729


# Test 2
line = ("3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,"
        "-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,"
        "53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10")

final_results = []
for perm in [[9, 7, 8, 5, 6]]:

    mems = [memory_from_string(line) for _ in range(5)]
    mem_addresses = [0] * len(mems)
    booster_io_manager = BoosterIoManager(perm)

    current_mem_index = 0

    while mem_addresses[-1] != -1:
        mem = mems[current_mem_index]
        addr = mem_addresses[current_mem_index]
        new_addr = resolve(mem, addr, booster_io_manager)

        mem_addresses[current_mem_index] = new_addr
        current_mem_index = (current_mem_index + 1) % len(mems)

    final_results.append(booster_io_manager.output_list[-1])

assert len(final_results) == 1 and final_results[0] == 18216


# Solution
relative_base = 0
with open("input.txt", "r") as fh:
    lines = fh.readlines()
assert len(lines) == 1
line = lines[0]

final_results = []
for perm in permutations([5, 6, 7, 8, 9]):
    
    mems = [memory_from_string(line) for _ in range(5)]
    mem_addresses = [0] * len(mems)
    booster_io_manager = BoosterIoManager(perm)

    current_mem_index = 0

    while mem_addresses[-1] != -1:
        mem = mems[current_mem_index]
        addr = mem_addresses[current_mem_index]
        new_addr = resolve(mem, addr, booster_io_manager)

        mem_addresses[current_mem_index] = new_addr
        current_mem_index = (current_mem_index + 1) % len(mems)

    final_results.append(booster_io_manager.output_list[-1])

print(final_results, max(final_results))

# for man in io_managers:
#     print(f"{man.input_list} -> {man.output_list}")
# print(max([man.output_list[-1] for man in io_managers]))