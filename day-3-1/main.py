def make_path(line_str, reverse_annotations=False):
    """
    :param reverse_annotations: Normally paths are labeled as (V)ertical or (H)orizontal,
                                but to intersect sets easier, this can be done in reverse.
    """
    DIRECTIONS = {
        "L": (-1, 0, 0),
        "R": (1, 0, 0),
        "U": (0, 1, 1),
        "D": (0, -1, 1)
    }

    DIR_ANNOTATIONS = ("H", "V")

    positions = []
    pos = (0, 0)

    for line_cmd in line_str:
        direction = DIRECTIONS[line_cmd[0]]
        annotation = DIR_ANNOTATIONS[(direction[2] + reverse_annotations) % 2]

        dist = int(line_cmd[1:])
        for d in range(1, dist):  # Exclude the endpoint
            positions.append((pos[0] + direction[0] * d,
                              pos[1] + direction[1] * d,
                              annotation))
        pos = (pos[0] + direction[0] * dist,
               pos[1] + direction[1] * dist)

    return set(positions)


# Test 1
test_line_1 = "R8,U5,L5,D3"
test_line_2 = "U7,R6,D4,L4"

path_1_str = test_line_1.rstrip().split(",")
path_2_str = test_line_2.rstrip().split(",")

path_1 = make_path(path_1_str)
path_2 = make_path(path_2_str, reverse_annotations=True)

common_points = path_1.intersection(path_2)
min_manhattan_dist = min([abs(a) + abs(b) for a, b, _ in common_points])
assert min_manhattan_dist == 6


# Test 2
test_line_1 = "R75,D30,R83,U83,L12,D49,R71,U7,L72"
test_line_2 = "U62,R66,U55,R34,D71,R55,D58,R83"

path_1_str = test_line_1.rstrip().split(",")
path_2_str = test_line_2.rstrip().split(",")

path_1 = make_path(path_1_str)
path_2 = make_path(path_2_str, reverse_annotations=True)

common_points = path_1.intersection(path_2)
min_manhattan_dist = min([abs(a) + abs(b) for a, b, _ in common_points])
assert min_manhattan_dist == 159


# Test 3
test_line_1 = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
test_line_2 = "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"

path_1_str = test_line_1.rstrip().split(",")
path_2_str = test_line_2.rstrip().split(",")

path_1 = make_path(path_1_str)
path_2 = make_path(path_2_str, reverse_annotations=True)

common_points = path_1.intersection(path_2)
min_manhattan_dist = min([abs(a) + abs(b) for a, b, _ in common_points])
assert min_manhattan_dist == 135


# Solution
with open("input.txt", "r") as fh:
    lines = fh.readlines()
    assert len(lines) == 2

path_1_str = lines[0].rstrip().split(",")
path_2_str = lines[1].rstrip().split(",")

path_1 = make_path(path_1_str)
path_2 = make_path(path_2_str, reverse_annotations=True)

common_points = path_1.intersection(path_2)
min_manhattan_dist = min([abs(a) + abs(b) for a, b, _ in common_points])
print(min_manhattan_dist)
