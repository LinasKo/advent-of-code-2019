class Node:
    def __init__(self, name):
        self.name = name
        self.children = set()
        self.parent = None

    def set_parent(self, parent):
        if self.parent != None:
            raise Exception("Already has a parent!")
        self.parent = parent

    def add_child(self, child):
        self.children.add(child)

    def count_descendants(self):
        count = len(self.children)
        for child in self.children:
            count += child.count_descendants()
        return count

    @staticmethod
    def find_root(node):
        """Assume that it's just one tree with only one root"""
        if node.parent is None:
            return node
        else:
            return Node.find_root(node.parent)


def find_orbit_count(node):
    count = node.count_descendants()
    for child in node.children:
        count += find_orbit_count(child)
    return count


def bfs(start_node, end_node):
    visited = set()
    to_expand = [(start_node, 0)]
    while len(to_expand) > 0:
        curr, dist = to_expand.pop(0)
        if curr in visited:
            continue

        if curr is end_node:
            return dist

        if curr.parent is not None:
            to_expand.append((curr.parent, dist + 1))
        for child in curr.children:
            to_expand.append((child, dist + 1))

        visited.add(curr)


nodes = {}
with open("input.txt", "r") as fh:
    for line in fh.readlines():
        [a, b] = line.rstrip().split(")")
        if a not in nodes:
            nodes[a] = Node(a)
        if b not in nodes:
            nodes[b] = Node(b)
        nodes[a].add_child(nodes[b])
        nodes[b].set_parent(nodes[a])

dist = bfs(nodes["YOU"], nodes["SAN"])
print(dist - 2)

