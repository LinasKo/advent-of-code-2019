from copy import deepcopy
import enum
from itertools import permutations


class Mode(enum.Enum):
    Position = "0"
    Immediate = "1"
    Relative = "2"


class Opcode(enum.Enum):
    Sum = 1
    Product = 2
    Input = 3
    Output = 4
    JumpIfNonzero = 5
    JumpIfZero = 6
    LessThan = 7
    Equals = 8
    ChangeRelativeBase = 9
    Terminate = 99


class BoosterIoManager:
    def __init__(self, input_list):
        assert len(input_list) == 5
        self.input_list = input_list
        self.input_pointer = 0
        self.output_list = [0]
        self.provide_output = False

    def get(self):
        if self.provide_output:
            ret = self.output_list[-1]
        else:
            ret = self.input_list[self.input_pointer]
            self.input_pointer += 1

        self.provide_output = not self.provide_output

        return ret

    def add_output(self, val):
        self.output_list.append(val)


def get_address(mem, ip, offset, mode):
    """
    :param mem: memory
    :param ip: int, instruction pointer, representing current address
    :param offset: int, offset from the instruction pointer to read the current value
    :param mode: Mode, parameter mode
    """
    if mode == Mode.Position.value:
        new_addr = mem[ip + offset]
    elif mode == Mode.Immediate.value:
        new_addr = ip + offset
    elif mode == Mode.Relative.value:
        new_addr = relative_base + mem[ip + offset]
    else:
        raise ValueError("Mode %d not recognized" % mode)

    if new_addr < 0:
        raise ValueError(f"Negative address produced.")
    return new_addr


def memory_from_string(string):
    memory = {}
    for i, val in enumerate(string.rstrip().split(",")):
        memory[i] = int(val)
    return memory


def dissect_instruction_head(instr):
    string = str(instr)
    opcode = int(string[-2:])
    if opcode == Opcode.Terminate.value:
        param_mode_length = 0

    elif opcode in [Opcode.Sum.value, Opcode.Product.value, Opcode.LessThan.value, Opcode.Equals.value]:
        param_mode_length = 3

    elif opcode in [Opcode.Input.value, Opcode.Output.value, Opcode.ChangeRelativeBase.value]:
        param_mode_length = 1

    elif opcode in [Opcode.JumpIfNonzero.value, Opcode.JumpIfZero.value]:
        param_mode_length = 2

    else:
        raise ValueError(f"Unrecognized opcode {opcode}")

    param_modes = []
    for i in range(1, param_mode_length+1):
        rev_index = -2 - i
        if -rev_index > len(string):
            param_modes.append(Mode.Position.value)
        else:
            param_modes.append(string[-2-i])

    return opcode, param_modes


def next_state(mem, addr, booster_io_manager):
    """
    Resolve one iteration of the program, updating memory in place.

    :param mem: list(int), array of numbers, will be updated in-place
    :param addr: int, address to start reading from
    :param booster_io_manager: IoManager, that passes in inputs and records outputs
    :return: Position to start reading from next time or -1
             if the program terminated successfully.
    """
    global relative_base
    opcode, modes = dissect_instruction_head(mem[addr])

    # # Debug
    # print(f"--- --- ---")
    # print(f"Mem: {mem}")
    # print(f"addr: {addr}, Op: {opcode}, Modes: {modes}, Rel base: {relative_base}")

    if opcode == Opcode.Terminate.value:
        return -1

    elif opcode == Opcode.Sum.value or opcode == Opcode.Product.value:
        m1 = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        m2 = mem.get(get_address(mem, addr, 2, modes[1]), 0)

        assert modes[2] != Mode.Immediate.value
        write_addr = get_address(mem, addr, 3, modes[2])

        if opcode == Opcode.Sum.value:
            mem[write_addr] = m1 + m2
        else:
            mem[write_addr] = m1 * m2

    elif opcode == Opcode.Input.value:
        assert modes[0] != Mode.Immediate.value
        write_addr = get_address(mem, addr, 1, modes[0])
        mem[write_addr] = booster_io_manager.get()

    elif opcode == Opcode.Output.value:
        out_val = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        booster_io_manager.add_output(out_val)

    elif opcode == Opcode.JumpIfNonzero.value or opcode == Opcode.JumpIfZero.value:
        condition = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        jump_addr = mem.get(get_address(mem, addr, 2, modes[1]), 0)

        if opcode == Opcode.JumpIfNonzero.value:
            if condition != 0:
                return jump_addr
        else:
            if condition == 0:
                return jump_addr

    elif opcode == Opcode.LessThan.value or opcode == Opcode.Equals.value:
        p1 = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        p2 = mem.get(get_address(mem, addr, 2, modes[1]), 0)

        assert modes[2] != Mode.Immediate.value
        write_addr = get_address(mem, addr, 3, modes[2])

        if opcode == Opcode.LessThan.value:
            mem[write_addr] = int(p1 < p2)
        else:
            mem[write_addr] = int(p1 == p2)

    elif opcode == Opcode.ChangeRelativeBase.value:
        relative_base += mem.get(get_address(mem, addr, 1, modes[0]), 0)

    else:
        raise ValueError(f"Unrecognized opcode '{opcode}'")

    return addr + len(modes) + 1


def resolve(mem, booster_io_manager):
    address = 0
    while address != -1:
        address = next_state(mem, address, booster_io_manager)


# Test 1
line = "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"
io_managers = []
for perm in permutations([0, 1, 2, 3, 4]):
    booster_io_manager = BoosterIoManager(perm)
    for _ in perm:
        mem = memory_from_string(line)
        resolve(mem, booster_io_manager)
    io_managers.append(booster_io_manager)
assert max([man.output_list[-1] for man in io_managers]) == 43210


# Test 2
line = "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0"
io_managers = []
for perm in permutations([0, 1, 2, 3, 4]):
    booster_io_manager = BoosterIoManager(perm)
    for _ in perm:
        mem = memory_from_string(line)
        resolve(mem, booster_io_manager)
    io_managers.append(booster_io_manager)
assert max([man.output_list[-1] for man in io_managers]) == 54321


# Test 3
line = "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0"
io_managers = []
for perm in permutations([0, 1, 2, 3, 4]):
    booster_io_manager = BoosterIoManager(perm)
    for _ in perm:
        mem = memory_from_string(line)
        resolve(mem, booster_io_manager)
    io_managers.append(booster_io_manager)
assert max([man.output_list[-1] for man in io_managers]) == 65210


# Solution
relative_base = 0
with open("input.txt", "r") as fh:
    lines = fh.readlines()
assert len(lines) == 1

io_managers = []
for perm in permutations([0, 1, 2, 3, 4]):
    booster_io_manager = BoosterIoManager(perm)
    for _ in perm:
        mem = memory_from_string(lines[0])
        resolve(mem, booster_io_manager)
    io_managers.append(booster_io_manager)

# for man in io_managers:
#     print(f"{man.input_list} -> {man.output_list}")
print(max([man.output_list[-1] for man in io_managers]))