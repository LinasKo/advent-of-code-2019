from copy import deepcopy
import enum


class Mode(enum.Enum):
    Position = "0"
    Immediate = "1"
    Relative = "2"


class Opcode(enum.Enum):
    Sum = 1
    Product = 2
    Input = 3
    Output = 4
    JumpIfNonzero = 5
    JumpIfZero = 6
    LessThan = 7
    Equals = 8
    ChangeRelativeBase = 9
    Terminate = 99


def get_address(mem, ip, offset, mode):
    """
    :param mem: memory
    :param ip: int, instruction pointer, representing current address
    :param offset: int, offset from the instruction pointer to read the current value
    :param mode: Mode, parameter mode
    """
    if mode == Mode.Position.value:
        new_addr = mem[ip + offset]
    elif mode == Mode.Immediate.value:
        new_addr = ip + offset
    elif mode == Mode.Relative.value:
        new_addr = relative_base + mem[ip + offset]
    else:
        raise ValueError("Mode %d not recognized" % mode)

    if new_addr < 0:
        raise ValueError(f"Negative address produced.")
    return new_addr


def memory_from_string(string):
    memory = {}
    for i, val in enumerate(string.rstrip().split(",")):
        memory[i] = int(val)
    return memory


def dissect_instruction_head(instr):
    string = str(instr)
    opcode = int(string[-2:])
    if opcode == Opcode.Terminate.value:
        param_mode_length = 0

    elif opcode in [Opcode.Sum.value, Opcode.Product.value, Opcode.LessThan.value, Opcode.Equals.value]:
        param_mode_length = 3

    elif opcode in [Opcode.Input.value, Opcode.Output.value, Opcode.ChangeRelativeBase.value]:
        param_mode_length = 1

    elif opcode in [Opcode.JumpIfNonzero.value, Opcode.JumpIfZero.value]:
        param_mode_length = 2

    else:
        raise ValueError(f"Unrecognized opcode {opcode}")

    param_modes = []
    for i in range(1, param_mode_length+1):
        rev_index = -2 - i
        if -rev_index > len(string):
            param_modes.append(Mode.Position.value)
        else:
            param_modes.append(string[-2-i])

    return opcode, param_modes


def next_state(mem, addr):
    """
    Resolve one iteration of the program, updating memory in place.

    :param mem: list(int), array of numbers, will be updated in-place
    :param addr: int, address to start reading from
    :return: Position to start reading from next time or -1
             if the program terminated successfully.
    """
    global relative_base
    opcode, modes = dissect_instruction_head(mem[addr])

    # # Debug
    # print(f"--- --- ---")
    # print(f"Mem: {mem}")
    # print(f"addr: {addr}, Op: {opcode}, Modes: {modes}, Rel base: {relative_base}")

    if opcode == Opcode.Terminate.value:
        return -1

    elif opcode == Opcode.Sum.value or opcode == Opcode.Product.value:
        m1 = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        m2 = mem.get(get_address(mem, addr, 2, modes[1]), 0)

        assert modes[2] != Mode.Immediate.value
        write_addr = get_address(mem, addr, 3, modes[2])

        if opcode == Opcode.Sum.value:
            mem[write_addr] = m1 + m2
        else:
            mem[write_addr] = m1 * m2

    elif opcode == Opcode.Input.value:
        assert modes[0] != Mode.Immediate.value
        write_addr = get_address(mem, addr, 1, modes[0])
        mem[write_addr] = int(input("Input a number: ").rstrip())

    elif opcode == Opcode.Output.value:
        out_val = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        print("Output:", out_val)

    elif opcode == Opcode.JumpIfNonzero.value or opcode == Opcode.JumpIfZero.value:
        condition = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        jump_addr = mem.get(get_address(mem, addr, 2, modes[1]), 0)

        if opcode == Opcode.JumpIfNonzero.value:
            if condition != 0:
                return jump_addr
        else:
            if condition == 0:
                return jump_addr

    elif opcode == Opcode.LessThan.value or opcode == Opcode.Equals.value:
        p1 = mem.get(get_address(mem, addr, 1, modes[0]), 0)
        p2 = mem.get(get_address(mem, addr, 2, modes[1]), 0)

        assert modes[2] != Mode.Immediate.value
        write_addr = get_address(mem, addr, 3, modes[2])

        if opcode == Opcode.LessThan.value:
            mem[write_addr] = int(p1 < p2)
        else:
            mem[write_addr] = int(p1 == p2)

    elif opcode == Opcode.ChangeRelativeBase.value:
        relative_base += mem.get(get_address(mem, addr, 1, modes[0]), 0)

    else:
        raise ValueError(f"Unrecognized opcode '{opcode}'")

    return addr + len(modes) + 1


def resolve(mem):
    address = 0
    while address != -1:
        address = next_state(mem, address)


# # Test 1 - returns 1125899906842624
# relative_base = 0
# lines = "104,1125899906842624,99"
# mem = memory_from_string(lines)
# resolve(mem)
# print(f"=== === ===")

# # Test 2 - returns 1219070632396864
# relative_base = 0
# lines = "1102,34915192,34915192,7,4,7,99,0"
# mem = memory_from_string(lines)
# resolve(mem)
# print(f"=== === ===")

# # Test 3 - returns 109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99
# relative_base = 0
# lines = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99"
# #       "109,1,204,-1,mem[100]++ ,m101 = m100 == 16 ,m101==0->0,99"  -> how do we cycle through values if m1 and m3 are const?????
# mem = memory_from_string(lines)
# resolve(mem)
# print(f"=== === ===")

# Solution
relative_base = 0
with open("input.txt", "r") as fh:
    lines = fh.readlines()
assert len(lines) == 1
mem = memory_from_string(lines[0])
resolve(mem)
