def fuel_req(mass):
   return  (int(mass) // 3) - 2

assert fuel_req(12) == 2
assert fuel_req(14) == 2
assert fuel_req(1969) == 654
assert fuel_req(100756) == 33583

total_cost = 0
with open("input.txt", "r") as fh:
    line = fh.readline()
    while line:
        line = line.rstrip()
        
        total_cost += fuel_req(line)

        line = fh.readline()

print(f"Total cost: {total_cost}")
